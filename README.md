# Radar cars controls


## About the project

> The dashboard displays data from car radar controls, including information such as speed, location, and time.
 
> This data is presented in multiple ways, including an interactive map and a table of raw data. 

> The map allows users to view the data in a visual format, with the ability to zoom in and out and pan to different locations. 

> The table, on the other hand, provides a detailed view of the data, allowing users to sort and filter the information as needed.

> The aim of this R Shiny dashboard is to provide an interactive interface for analyzing radar controls information. Users can filter and explore data based on various parameters, visualize radar controls, and access summary statistics and data tables. The dashboard enables focused analysis by selecting specific regions of interest. The goal is to facilitate data exploration and informed decision-making regarding radar controls.



## About the data


> The data includes radars controls info from 2021 in France. The radars are mesured by radars located on few cars on the roads of France.

> Because of a large amount of data, we couldn't plot every points at the same time, so you can only filter the data by week. It means that if you want to find a particular region if not in the week 1, you can go in the second page, look for the particular region and look to the date and go back to the interactive dashboard selecting the right week.


## About me

> My name is Jean ROBIN, I'm a data analyst.
 
> I just spent 6 months in a swedish FinTech as a data analyst intern for the last semester of my master degree in data science.

> I'm highly passionate by creating data vizualisations to emphasize relevant insights. 

> I'm looking for a job as a data analyst or business analyst.
