library(shiny)
library(leaflet)
library(readr)
library(dplyr)
library(shinydashboard)
library(htmlwidgets)
library(shinythemes)
library(plotly)
library(shinycssloaders)
library(ggplot2)
library(ggExtra)
library("data.table")
library(cowplot)

# ============== reste à faire ================
# barplot, stacked barplot avec les différents degrés d'exces
# ajouter le nombre d'exces de vitesse par region et par heure sur les barplots
# légende
# ajouter un filtre sur les intervalles d'exces
# penser à l'idée de créer une 4ème page, une de synthèse avec les résultats en textOutput et plot
# sur l'ensemble des données, mais aussi avec des filtres sur les mois

# =============================================

# setwd('Documents/PERSO/PROJETS/dashboards-shiny/projet-radars/radar-cars-controls/scripts/')
radars = read_csv('../../data/radars-cleaned2.csv')
radars = na.omit(radars)


library(ggplot2)
library(scales)

# Function factory for secondary axis transforms
train_sec <- function(primary, secondary, na.rm = TRUE) {
  # Thanks Henry Holm for including the na.rm argument!
  from <- range(secondary, na.rm = na.rm)
  to   <- range(primary, na.rm = na.rm)
  # Forward transform for the data
  forward <- function(x) {
    rescale(x, from = from, to = to)
  }
  # Reverse transform for the secondary axis
  reverse <- function(x) {
    rescale(x, from = to, to = from)
  }
  list(fwd = forward, rev = reverse)
}



ui = bootstrapPage(
  tags$link(
    rel="stylesheet", 
    href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap" 
  ),
  tags$style(HTML('
      .box {
        # border: 1px solid #3182bd;
        #border-radius: 0.25rem;
        #padding: 1rem;
        margin-bottom: 0.5rem;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        width: 100%;
        text-align: center;
        
      }
      .boxText {
        # border: 1px solid #3182bd;
        #border-radius: 0.25rem;
        #padding: 1rem;
        margin-bottom: 2rem;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        width: 100%;
        font-family: "Quicksand", sans-serif;
        font-size: 32px;
        text-align: center;
      }
      .boxPlot {
        # border: 1px solid #3182bd;
        #border-radius: 0.5rem;
        #padding: 1rem;
        margin-bottom: 1rem;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        width: 100%;
        font-family: "Quicksand", sans-serif;
        font-size: 16px;
      }
      .box-header {
        background-color: #f05424;
        color: white;
        padding: 0.5rem 1rem;
        border-top-left-radius: 0.25rem;
        border-top-right-radius: 0.25rem;
        font-family: "Quicksand", sans-serif;
        font-size: 16px;
        font-weight: bold;
      }
    ')),
  
  navbarPage(theme = shinytheme('united'),"Radar Cars Controls in France in 2021", id="main",
             tabPanel("Interactive Dashboard", 
                      fluidRow(
                        column(width = 7,
                               div(class = "box",
                                   div(class = "box-header", ""),
                                   leafletOutput(outputId = 'map', width = '100%', height = 873),
                                   #uiOutput("legend_box")
                               ),
                               
                        
                        ),
                        column(width = 5,
                          sidebarPanel(width = '50%',
                               fluidRow(
                                 column(width = 6,
                                        numericInput('week_filter', 'Week Number', value = 1, min = 1, max = 53, 
                                                     width = '100%'),
                                        checkboxInput('speeding_checkbox', value = FALSE, label = 'Only Speeding'),
                                        
                                 ),
                                 column(
                                   width = 6,
                                   selectInput('selectRegion', 'Region', choices = c('All')),
                                   checkboxInput('percentages_checkbox', value = FALSE, label = 'Percentages'),
                                 ),
                               ),
                          ),
                          
                          
                          fluidRow(
                            column(
                              width = 3,
                              div(class = "boxText",
                                  div(class = "box-header", h5("Controls")),
                                  textOutput("number_of_checks")
                              ),
                            ),
                            column(
                              width = 3,
                              div(class = "boxText",
                                  div(class = "box-header", h5("Speedings"),
                                  ),
                                  textOutput("number_of_speedings")
                              ),
                            ),
                            column(
                              width = 3,
                              div(class = "boxText",
                                  div(class = "box-header", h5("Maximal speed")),
                                  textOutput("maximal_speed")
                              ),
                            ),
                            column(
                              width = 3,
                              div(class = "boxText",
                                  div(class = "box-header", h5("Maximal speeding")),
                                  textOutput("max_speeding"),
                              ),
                            ),
                          ),

                          fluidRow(
                            column(width=12,
                                   # div(class = "boxPlot",
                                   #     div(class = "box-header", h5("Controls speeding distribution")),
                                   #     plotlyOutput('hist_plot', height = 266),
                                   # ),
                                   div(class = "boxPlot",
                                       div(class = "box-header", h5("Percentage of speeding by region")),
                                       plotlyOutput(outputId = 'barplot', height = 249),
                                   ),
                                   div(class = 'boxPlot',
                                       div(class = 'box-header', h5('Percentage of speeding by hour')),
                                       plotlyOutput(outputId = 'barplot2', height = 249)),
                            ),
                          ),
                          
                        )
                      )
                      ),
             
             tabPanel("Weekly Control Counts", 
                      div(class = "box",
                          div(class = "box-header", "Number of radars controls per week by region"),
                          plotlyOutput("controlsPlot", height = 850) %>% withSpinner(color='#f05424'),
                      ),
                     ),

                    
             
             
             
             
            
             tabPanel("Data Explorer", DT::DTOutput("table") %>% withSpinner(color='#f05424')),
             #tabPanel("Data", DT::dataTableOutput("table"))
             
             tabPanel("Learn More",
                      includeMarkdown('../README.md')
             )
  )
)

server = function(input, output, session){
  
  filtered_data = reactive({
    if(input$speeding_checkbox[1] == TRUE){
      filtered = radars[radars$week == input$week_filter[1] & radars$speeding_bool == 1, ]
    }
    else{
      filtered = radars[radars$week == input$week_filter[1], ]
    }
    filtered
  })
  
  filteredDataByRegion <- reactive({
    selectedRegion <- input$selectRegion
    
    if (selectedRegion == "All") {
      # Return the unfiltered data
      filtered_data()
    } else {
      # Filter the data by the selected region
      filteredByRegion <- filtered_data()[filtered_data()$region_name == selectedRegion, ]
      return(filteredByRegion)
    }
  })
  
  observe({
    # Get the unique regions from the filtered data
    regions <- unique(filtered_data()$region_name)
    
    # Update the choices of the select input
    updateSelectInput(session, "selectRegion", choices = c('All', regions))
  })
  
  
  
  # init map
  output$map <- renderLeaflet({
    leaflet(data = filteredDataByRegion()) %>% 
      addTiles() %>%
      setView(-1.5, 48.5, zoom = 7) 
  })
  
  # output$legend_box <- renderUI({
  #   box(title = "Custom Legend", width = 250, status = "primary", solidHeader = TRUE,
  #       leaflet::addLegend("topright", values = c(1,10),
  #                          title = "My Legend"))
  # })
  
  # update map
  observe({
    leafletProxy('map', data = filteredDataByRegion()) %>%
      clearMarkers() %>%
      addCircles(data = filteredDataByRegion(), lat = ~lat, lng = ~long, fillOpacity = .5, color = ~color, #~pal(exces),
                 label = sprintf("<b>Speeding:</b> %g<br/><b>Speed:</b> %d<br/><b>Limit:</b> %g<br/><b>Date:</b> %s
                                 <br/><b>Hour:</b> %s", filtered_data()$exces,
                                 filtered_data()$mesure, filtered_data()$limite, filtered_data()$day,
                                 filtered_data()$hour) %>% lapply(htmltools::HTML),
                 # labelOptions = labelOptions(
                 #   style = list('font-family' = 'Courier', 'font-size' = 12, "color" = "#f05424",
                 #                direction = "auto")
                 # ),
                 labelOptions = labelOptions(
                   style = list("font-weight" = "normal", padding = "3px 8px", "color" = '#f05424'),
                   textsize = "15px", direction = "auto", 'font-family' = 'Courier')
      )
    })
  
  output$number_of_checks <- renderText({
    paste0(length(filteredDataByRegion()$mesure))
  })
  
  output$number_of_speedings <- renderText({
    if(input$percentages_checkbox[1]==FALSE) {
      paste0(sum(filteredDataByRegion()$speeding_bool))
    }
    else{
      paste0(round(sum(filteredDataByRegion()$speeding_bool)/length(filteredDataByRegion()$mesure)*100, 1)
             , '%')
    }
  })

  output$maximal_speed <- renderText({
      paste0(max(filteredDataByRegion()$mesure), " km/h")
  })
  
  output$max_speeding <- renderText({
    if(input$percentages_checkbox[1]==FALSE) {
      paste0(max(filteredDataByRegion()$exces[filteredDataByRegion()$speeding_bool == 1]), " km/h")
    }else{
      paste0('+', round(max(filteredDataByRegion()$exces*100/filteredDataByRegion()$limite), 1), '%')
    }
  })
  
  # output$hist_plot = renderPlotly({
  #   ggplotly(ggplot(filtered_data(), aes(x=exces)) + 
  #     geom_histogram(color="#f05424", fill="#f05424", binwidth = 1) + removeGrid() + 
  #       xlab('') + ylab('')) %>% 
  #     layout(xaxis = list(fixedrange = TRUE)) %>%
  #     layout(yaxis = list(fixedrange = TRUE)) %>%
  #     config(displayModeBar = F) 
  # })

  
  # ====================================================================
  # barplot pourcentages exces par région
  # ====================================================================
  filtered_data_for_barplot <- reactive({
    labels = unique(filtered_data()$region_name)
    labels = setdiff(labels, NA)
    values = c()
    n_checks_list = c()
    for(region in labels){
      df_temp = filtered_data()[filtered_data()$region_name == region, ]
      n_checks = dim(df_temp)[1]
      n_checks_list = c(n_checks_list, n_checks)
      n_speedings = sum(df_temp$speeding_bool, na.rm = TRUE)
      perc = round(n_speedings/n_checks*100, 1)
      values = c(values, perc)
    }
    df_barplot = data.frame(labels, values, n_checks_list)
    df_barplot = df_barplot %>% arrange(desc(values))
    df_barplot$id = seq_along(df_barplot$labels)  # Add a unique identifier
    print(df_barplot)
    df_barplot
    # df_barplot = df_barplot2[order(df_barplot2$values, decreasing = TRUE), ]
    # return(df_barplot)
  })
  
 
  output$barplot = renderPlotly({
    p <- ggplot(filtered_data_for_barplot(), aes(x=reorder(labels, id))) +
      geom_bar(aes(y=values, text = sprintf("%.1f", values)), stat="identity", size=.1, fill="#f05424", color="black", alpha=.4) + 
      geom_point(aes(y=n_checks_list/(max(filtered_data_for_barplot()$n_checks_list)/max(filtered_data_for_barplot()$values)), 
                    text = sprintf("%.0f", n_checks_list)), size=2, color="black", shape = 3) +
      removeGrid() +
      xlab("") +
      ylab("Percentage of speeding") +
      scale_y_continuous(
        sec.axis = sec_axis(~. * (max(filtered_data_for_barplot()$n_checks_list)/max(filtered_data_for_barplot()$values)), name="Number of controls")
      ) +
      scale_fill_manual(values = c("#f05424", "black"), labels = c("Percentage of speedings", "Number of controls")) +
      theme(
        legend.position = "bottom",
        legend.margin=margin(-5,0,0,0),
        axis.title.y = element_text(color = "#f05424", size=8),
        axis.title.y.right = element_text(color = "black", size=13),
        plot.background = element_rect(fill = "white"),
        panel.background = element_rect(fill = "white")
      )
    
    ggplotly(p, tooltip = "text") %>%  layout(xaxis = list(fixedrange = TRUE)) %>%
      layout(yaxis = list(fixedrange = TRUE)) %>%
      config(displayModeBar = FALSE)  
  })

  
  
  # ====================================================================
  # barplot pourcentages exces par heure
  # ====================================================================
  
  filtered_data_for_barplot_hour = reactive({
    labels = sort(unique(filteredDataByRegion()$hour2))
    # print(labels)
    percentage = c()
    hour = c()
    nb_checks = c()
    for (h in labels){
      df_temp = filteredDataByRegion()[(filteredDataByRegion()$hour2 == h), ] #| (filteredDataByRegion()$hour2 == h+1), ]
      n_checks = dim(df_temp)[1]
      n_speedings = sum(df_temp$speeding_bool, na.rm = TRUE)
      perc = round(n_speedings/n_checks*100, 1)
      percentage = c(percentage, perc)
      hour = c(hour, h)
      nb_checks = c(nb_checks, n_checks)
    }
    
    df_barplot = data.frame(hour, percentage, nb_checks)
    print(df_barplot)
  })
  
  # output$barplot2 = renderPlotly(
  #   # ggplotly(ggplot(filtered_data_for_barplot_hour(), aes(x = hour, y = percentage)) +
  #   #            geom_bar(stat = "identity", fill = "#f05424") +
  #   #            xlab("") +
  #   #            ylab("") + 
  #   #            removeGrid()) %>% 
  #   #   layout(xaxis = list(fixedrange = TRUE)) %>%
  #   #   layout(yaxis = list(fixedrange = TRUE)) %>%
  #   #   config(displayModeBar = F) %>%
  #   #   layout()
  #   
  #   ggplotly(ggplot(filtered_data_for_barplot_hour(), aes(x = hour)) +
  #              geom_bar(aes(y = percentage), stat = "identity", fill = "#f05424", size=.1, color="black", alpha=.4) +
  #              geom_line(aes(y = nb_checks/(max(filtered_data_for_barplot_hour()$nb_checks)/max(filtered_data_for_barplot_hour()$hour)),
  #                            text = sprintf("%.0f", nb_checks)
  #                            ),
  #                         ) +
  #              xlab("") +
  #              ylab("") + 
  #              ylab("Percentage of speeding") +
  #              scale_y_continuous(
  #                sec.axis = sec_axis(~. * (max(filtered_data_for_barplot_hour()$nb_checks)/max(filtered_data_for_barplot_hour()$hour)), name="Number of controls")
  #              ) +
  #              theme(
  #                legend.position = "bottom",
  #                legend.margin=margin(-5,0,0,0),
  #                axis.title.y = element_text(color = "#f05424", size=8),
  #                axis.title.y.right = element_text(color = "black", size=13),
  #                plot.background = element_rect(fill = "white"),
  #                panel.background = element_rect(fill = "white")
  #              )+
  #              removeGrid()) %>% 
  #     layout(xaxis = list(fixedrange = TRUE)) %>%
  #     layout(yaxis = list(fixedrange = TRUE)) %>%
  #     config(displayModeBar = F) %>%
  #     layout()
  #   
  # )
  
  output$barplot2 = renderPlotly({
    p <- ggplot(filtered_data_for_barplot_hour(), aes(x=hour)) +
      geom_bar(aes(y=percentage, text = sprintf("%.1f", percentage)), stat="identity", size=.1, fill="#f05424", color="black", alpha=.4) + 
      geom_point(aes(y=nb_checks/(max(filtered_data_for_barplot_hour()$nb_checks)/max(filtered_data_for_barplot_hour()$percentage)), 
                     text = sprintf("%.0f", nb_checks)), size=2, color="black", shape=3) +
      removeGrid() +
      xlab("") +
      ylab("Percentage of speeding") +
      scale_y_continuous(
        sec.axis = sec_axis(~. * (max(filtered_data_for_barplot_hour()$nb_checks)/max(filtered_data_for_barplot_hour()$percentage)), 
                            name="Number of controls")
      ) +
      scale_fill_manual(values = c("#f05424", "black"), labels = c("Percentage of speedings", "Number of controls")) +
      theme(
        legend.position = "bottom",
        legend.margin=margin(-5,0,0,0),
        axis.title.y = element_text(color = "#f05424", size=8),
        axis.title.y.right = element_text(color = "black", size=13),
        plot.background = element_rect(fill = "white"),
        panel.background = element_rect(fill = "white")
      )
    
    ggplotly(p, tooltip = "text") %>%  layout(xaxis = list(fixedrange = TRUE)) %>%
      layout(yaxis = list(fixedrange = TRUE)) %>%
      config(displayModeBar = FALSE)  
  })
  
  
  
  # output$controlsPlot <- renderPlot({
  #   # Plot the number of radar controls per week, grouped by region
  #   ggplot(radars, aes(x = week, y = ..count.., fill = region_name)) +
  #     geom_bar(stat = "count") +
  #     labs(x = "Week", y = "Number of Radar Controls", fill = "Region") +
  #     theme_minimal()
  # })
  
  
  output$controlsPlot <- renderPlotly({
    # Create a plotly object and plot the radar controls data
    p <- ggplot(radars, aes(x = week, y = ..count.., fill = region_name)) +
      geom_bar(stat = "count") +
      labs(x = "Week", y = "Number of Radar Controls", fill = "Region") +
      theme_minimal() + removeGrid()
    
    # Convert ggplot object to plotly
    p <- ggplotly(p) %>% 
      layout(xaxis = list(fixedrange = TRUE)) %>%
      layout(yaxis = list(fixedrange = TRUE)) %>%
      config(displayModeBar = F) %>%
      layout()
    
    # Enable hover information
    p <- p %>% event_register("plotly_hover")
    
    # Update the plot based on the selected region
    # p <- p %>% event_data("plotly_hover") %>%
    #   filter(!is.na(region_name)) %>%
    #   filter(region_name %in% unique(radars$region_name))
    # 
    p
  })
  
  
  # 2nd panel
  output$table = DT::renderDT(radars[, -c(7, 8, 9)], filter = 'top', options = list(pageLenght = 15))
  
}

shinyApp(ui, server)


